package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Player;

public class PlayerDAO {

	public static ArrayList<Player> getRandPlayer(int limit) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM player ORDER BY RAND() LIMIT ? ");
			st.setInt(1, limit);

			ResultSet rs = st.executeQuery();

			ArrayList<Player> playerList = new ArrayList<Player>();

			while (rs.next()) {
				Player player = new Player();
				player.setId(rs.getInt("id"));
				player.setName(rs.getString("name"));
				player.setFileName(rs.getString("file_name"));
				player.setTeamId(rs.getInt("team_id"));
				player.setManufacturerId(rs.getInt("manufacturer_id"));
				player.setItemId(rs.getInt("item_id"));
				playerList.add(player);
			}
			System.out.println("getAllPlayer completed");
			return playerList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static ArrayList<Player> getPlayersByPlayerName(String searchWord, int pageNum, int pageMaxPlayerCount) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			int startiPlayerNum = (pageNum - 1) * pageMaxPlayerCount;
			con = DBManager.getConnection();

			if (searchWord.length() == 0) {
				// 全検索
				st = con.prepareStatement("SELECT * FROM player ORDER BY id ASC LIMIT ?,? ");
				st.setInt(1, startiPlayerNum);
				st.setInt(2, pageMaxPlayerCount);
			} else {
				// 商品名検索
				st = con.prepareStatement("SELECT * FROM player WHERE name LIKE ?  ORDER BY id ASC LIMIT ?,? ");
				st.setString(1,"%" + searchWord + "%");
				st.setInt(2, startiPlayerNum);
				st.setInt(3, pageMaxPlayerCount);

				/*"SELECT * FROM player"
				+ " JOIN team"
				+ " ON player.team_id = team.id"
				+ " WHERE player.id = ?");

				"SELECT * FROM player"
				+ " JOIN manufacturer"
				+ " ON player.manufacturer_id = manufacturer.id"
				+ " WHERE player.id = ?");*/
			}

			ResultSet rs = st.executeQuery();
			ArrayList<Player> playerList = new ArrayList<Player>();

			while (rs.next()) {
				Player player = new Player();
				player.setId(rs.getInt("id"));
				player.setName(rs.getString("name"));
				player.setFileName(rs.getString("file_name"));
				player.setTeamId(rs.getInt("team_id"));
				player.setManufacturerId(rs.getInt("manufacturer_id"));
				player.setItemId(rs.getInt("item_id"));

				playerList.add(player);
			}
			System.out.println("get Players by playerName has been completed");
			return playerList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static double getPlayerCount(String searchWord) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("select count(*) as cnt from player where name like ?");
			st.setString(1, "%" + searchWord + "%");
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static Player getPlayerID(int playerId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM player WHERE id = ?");
			st.setInt(1, playerId);

			ResultSet rs = st.executeQuery();

			Player player = new Player();
			if (rs.next()) {
				player.setId(rs.getInt("id"));
				player.setName(rs.getString("name"));
				player.setFileName(rs.getString("file_name"));
				player.setTeamId(rs.getInt("team_id"));
				player.setManufacturerId(rs.getInt("manufacturer_id"));
				player.setItemId(rs.getInt("item_id"));
			}

			System.out.println("searching item by itemID has been completed");

			return player;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

}
