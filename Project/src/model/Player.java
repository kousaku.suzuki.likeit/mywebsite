package model;

import java.io.Serializable;

public class Player implements Serializable {
	private int id;
	private String name;
	private String fileName;
	private int teamId;
	private int manufacturerId;
	private int itemId;


	public Player(int id, String name, String fileName, int teamId, int manufacturerId, int itemId) {
		super();
		this.id = id;
		this.name = name;
		this.fileName = fileName;
		this.teamId = teamId;
		this.manufacturerId = manufacturerId;
		this.itemId = itemId;
	}

	public Player() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public int getId() {
		return id;
	}

	public void setId(int itemId) {
		this.id = itemId;
	}

	public String getName() {
		return name;
	}

	public void setName(String itemName) {
		this.name = itemName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getTeamId() {
		return teamId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}

	public int getManufacturerId() {
		return manufacturerId;
	}

	public void setManufacturerId(int manufacturerId) {
		this.manufacturerId = manufacturerId;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

}