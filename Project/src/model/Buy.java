package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Buy implements Serializable {
	private int id;
	private int userId;
	private int totalPrice;
	private Date buyDate;

	public Buy() {

	};

	public Buy(int id, int userId, int totalPrice, Date buyDate) {
		this.id = id;
		this.userId = userId;
		this.totalPrice = totalPrice;
		this.buyDate = buyDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Date getBuyDate() {
		return buyDate;
	}

	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}

	public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
		return sdf.format(buyDate);
	}

	/*public String getFormatTotalPrice() {
		return String.format("%,d", this.totalPrice + this.deliveryMethodPrice);
	}*/
}
