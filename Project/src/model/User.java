package model;

import java.io.Serializable;

public class User implements Serializable {
	private String name;
	private String address;
	private String loginId;
	private String password;
	private int id;
	private String createDate;


	public User(int id, String loginId, String name) {
		super();
		this.name = name;
		this.loginId = loginId;
		this.id = id;
	}

	// 全てのデータをセットするコンストラクタ
		public User(int id, String name, String address, String loginId,   String password, String createDate) {
			this.id = id;
			this.name = name;
			this.address = address;
			this.loginId = loginId;
			this.password = password;
			this.createDate = createDate;
		}

	public User() {
			// TODO 自動生成されたコンストラクター・スタブ
		}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public void setUpdateUserInfo(String name, String loginId, String address, int id) {
		this.name = name;
		this.loginId = loginId;
		this.address = address;
		this.id = id;
	}

}
