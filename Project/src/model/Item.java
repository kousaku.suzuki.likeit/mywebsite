package model;

import java.io.Serializable;

public class Item implements Serializable {
	private int id;
	private String name;
	private String manufacturerId;
	private int price;
	private String fileName;

	public Item() {
	}

	public Item(int id, String name, String manufacturerId, int price, String fileName) {
		super();
		this.id = id;
		this.name = name;
		this.manufacturerId = manufacturerId;
		this.price = price;
		this.fileName = fileName;
	}

	public int getId() {
		return id;
	}

	public void setId(int itemId) {
		this.id = itemId;
	}

	public String getName() {
		return name;
	}

	public void setName(String itemName) {
		this.name = itemName;
	}

	public String getManufacturerId() {
		return manufacturerId;
	}

	public void setManufacturerId(String manufacturerId) {
		this.manufacturerId = manufacturerId;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int itemPrice) {
		this.price = itemPrice;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String filename) {
		this.fileName = filename;
	}

	public String getFormatPrice() {
		return String.format("%,d", this.price);
	}

}