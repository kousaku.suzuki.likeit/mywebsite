package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.User;

/**
 * Servlet implementation class myPageUpdateConfirmServlet
 */
@WebServlet("/myPageUpdateConfirmServlet")
public class myPageUpdateConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");

		//セッション開始
		HttpSession session = request.getSession();
		try {
			//エラーメッセージを格納する変数
			String validationMessage = "";

			//入力フォームから受け取った値をBeansにセット
			User udb = new User();
			udb.setUpdateUserInfo(request.getParameter("name"),request.getParameter("login_id"),request.getParameter("address"), (int)((User) (session.getAttribute("userId"))).getId());


			//ログインIDの入力規則チェック 英数字 ハイフン アンダースコアのみ入力可能
			if (!EcHelper.isLoginIdValidation(udb.getLoginId())) {
				validationMessage = "半角英数とハイフン、アンダースコアのみ入力できます";
			}
			//loginIdの重複をチェック
			/*if ( UserDAO.isOverlapLoginId(udb.getLoginId(),(int)((User) (session.getAttribute("userId"))).getId())) {
				validationMessage = "ほかのユーザーが使用中のログインIDです";
			}*/

			//バリデーションエラーメッセージがないなら確認画面へ
			if(validationMessage.length() == 0){
				//確認画面へ
				request.setAttribute("udb",udb);
				request.getRequestDispatcher(EcHelper.ACCOUNT_UPDATE_CONFIRM_PAGE).forward(request, response);
			}else {
				//セッションにエラーメッセージを持たせてユーザー画面へ
				session.setAttribute("validationMessage", validationMessage);
				response.sendRedirect("myPageServlet");
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
