package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.BuyDAO;
import dao.BuyDetailDAO;
import model.Buy;
import model.Item;

/**
 * Servlet implementation class BuyHistory
 */
@WebServlet("/BuyHistory")
public class BuyHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			int id = Integer.parseInt(request.getParameter("buy_id"));

			BuyDAO buyDao = new BuyDAO();
			Buy history = buyDao.findByDetail(id);
			request.setAttribute("history", history);

			List<Item> itemList = BuyDetailDAO.getItemListByBuyId(id);
			request.setAttribute("itemList", itemList);

			request.getRequestDispatcher(EcHelper.BUY_HISTORY).forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
