package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.PlayerDAO;
import model.Player;

/**
 * Servlet implementation class PlayerList
 */
@WebServlet("/PlayerList")
public class PlayerList extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//1ページに表示する商品数
		final static int PAGE_MAX_PLAYER_COUNT = 8;

		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			HttpSession session = request.getSession();
			try {

				String searchWord = request.getParameter("search_word");
				//表示ページ番号 未指定の場合 1ページ目を表示
				int pageNum = Integer.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));
				// 新たに検索されたキーワードをセッションに格納する
				session.setAttribute("searchWord", searchWord);

				// 商品リストを取得 ページ表示分のみ
				ArrayList<Player> searchResultPlayerList = PlayerDAO.getPlayersByPlayerName(searchWord, pageNum, PAGE_MAX_PLAYER_COUNT);

				// 検索ワードに対しての総ページ数を取得
				double playerCount = PlayerDAO.getPlayerCount(searchWord);
				int pageMax = (int) Math.ceil(playerCount / PAGE_MAX_PLAYER_COUNT);

				//総アイテム数
				request.setAttribute("playerCount", (int) playerCount);
				// 総ページ数
				request.setAttribute("pageMax", pageMax);
				// 表示ページ
				request.setAttribute("pageNum", pageNum);
				request.setAttribute("playerList", searchResultPlayerList);

				request.getRequestDispatcher(EcHelper.SEARCH_RESULT_PAGE).forward(request, response);
			} catch (Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("Error");
			}
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
