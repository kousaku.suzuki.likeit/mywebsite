package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BuyDAO;
import dao.UserDAO;
import model.Buy;
import model.User;

/**
 * Servlet implementation class myPageServlet
 */
@WebServlet("/myPageServlet")
public class myPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/*HttpSession session = request.getSession();
		try {
			User user = (User) session.getAttribute("userId");
			int userId = (int)((User) (session.getAttribute("userId"))).getId();


			if (user == null) {

				response.sendRedirect("HomeServlet");
				return;
			}

			String loginId = request.getParameter("userId");

			UserDAO userDao = new UserDAO();
			User udb = userDao.findByDetail(loginId);

			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("udb", udb);

			//DAOで実行したSQL文から取得したデータを代入
			List<Buy> buydataList = BuyDAO.getBuyByUserId(userId);
			//リクエストスコープに保存
			request.setAttribute("buydataList", buydataList);

			RequestDispatcher dispatcher = request.getRequestDispatcher(EcHelper.ACCOUNT_PAGE);
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}*/
		// セッション開始
		HttpSession session = request.getSession();
		try {
			// ログイン時に取得したユーザーIDをセッションから取得
			int userId = (int) ((User) (session.getAttribute("userId"))).getId();
			// 更新確認画面から戻ってきた場合Sessionから取得。それ以外はuserIdでユーザーを取得
			User udb = session.getAttribute("returnUDB") == null ? UserDAO.getUserByUserId(userId)
					: (User) EcHelper.cutSessionAttribute(session, "returnUDB");

			// 入力された内容に誤りがあったとき等に表示するエラーメッセージを格納する
			String validationMessage = (String) EcHelper.cutSessionAttribute(session, "validationMessage");
			request.setAttribute("validationMessage", validationMessage);
			request.setAttribute("udb", udb);

			//DAOで実行したSQL文から取得したデータを代入
			List<Buy> buydataList = BuyDAO.getBuyByUserId(userId);
			//リクエストスコープに保存
			request.setAttribute("buydataList", buydataList);

			request.getRequestDispatcher(EcHelper.ACCOUNT_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
