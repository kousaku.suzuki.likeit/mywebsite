<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>アカウント情報/更新確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	 crossorigin="anonymous">
<jsp:include page="/baselayout/head.jsp" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<br>
	<div class="container">
		<div class="row col-3 mx-auto">
			<h5 class=" col s12 light">入力内容確認</h5>
		</div>
		<div class="row">
			<div class="col s12">
				<div class="card grey lighten-5">
					<div class="card-content">
						<form action="myPageUpdateResultServlet" method="POST">
							<div class="row">
								<div class="input-field col s6">
									<label>名前</label><input type="text" name="user_name_update" value="${udb.name}" readonly>
								</div>
								<div class="input-field col s6">
									<label>ログインID</label><input type="text" name="login_id_update" value="${udb.loginId}" readonly>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s12">
									<label>住所</label><input type="text" name="user_address_update" value="${udb.address}" readonly>
								</div>
							</div>
							<div class="row col-5 mx-auto">
								<div class="col s12">
									<p class="center-align">上記内容で更新してよろしいでしょうか?</p>
								</div>
							</div>
                            <div class="row col-4 mx-auto">
								<div class="col s6 center-align">
                                    <button class="btn btn-info" type="submit" name="confirmButton" value="cancel">戻る</button>
								</div>
								<div class="col s6 center-align">
									<button class="btn btn-info" type="submit" name="confirmButton" value="update">更新</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>