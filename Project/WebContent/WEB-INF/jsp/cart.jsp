<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>カート確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<jsp:include page="/baselayout/head.jsp" />

</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<br>
	<div class="container">
		<div class="row col-2 mx-auto">${cartActionMessage}</div>
		<div class="row col-2 mx-auto">
			<h5 class=" col s12 light">カート</h5>
		</div>
		<div class="section">
			<form action="ItemDelete" method="POST">
				<div class="row col-4 mx-auto">
					<div class="col s6 center-align">
						<button class="btn btn-info" type="submit" name="action">削除</button>
					</div>
					<div class="col s6 center-align">
						<a href="Buy" class="btn btn-info">レジに進む</a>
					</div>
				</div>
				<div class="row">
					<c:forEach var="item" items="${cart}" varStatus="status">
						<div class="col s12 m3">
							<div class="card">
								<div class="card-image">
									<a href="Item?item_id=${item.id}"><img
										src="img/${item.fileName}"> </a>
								</div>
								<div class="card-content">
									<span class="card-title">${item.name}</span>
									<p>${item.price}円</p>
									<p>
										<input type="checkbox" id="${status.index}"
											name="delete_item_id_list" value="${item.id}" /> <label
											for="${status.index}">削除</label>
									</p>
								</div>
							</div>
						</div>
					</c:forEach>
				</div>

			</form>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>