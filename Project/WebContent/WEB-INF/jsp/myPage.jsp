<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>アカウント情報</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	 crossorigin="anonymous">
<jsp:include page="/baselayout/head.jsp" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<br>
	<div class="container">
		<div class="row col-3 mx-auto">
			<h5 class=" col s12 light">アカウント情報</h5>
		</div>
		<div class="row">
			<div class="col s12">
				<div class="card grey lighten-5">
					<div class="card-content">
						<form action="myPageUpdateConfirmServlet" method="POST">
							<c:if test="${validationMessage != null}">
								<p class="red-text center-align">${validationMessage}</p>
							</c:if>
							<br> <br>
							<div class="row">
								<div class="input-field col s6">
                                    <input type="text" name="name" value="${udb.name}">
								</div>
								<div class="input-field col s6">
									<input type="text" name="login_id" value="${udb.loginId}">
								</div>
							</div>
							<div class="row">
								<div class="input-field col s12">
									<input type="text" name="address" value="${udb.address}">
								</div>
							</div>
							<div class="row center">
								<div class="col s12">
									<button class="btn btn-info" type="submit" name="action">　更新画面へ　</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--  購入履歴 -->
        <div class="row">
			<div class="col s12">
				<div class="card grey lighten-5">
					<div class="card-content">
						<table class="bordered">
							<thead>
								<tr>
									<th style="width: 10%"></th>
									<th class="center">購入日時</th>
									<th class="center">購入金額</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="bdb" items="${buydataList}" >
									<tr>
										<td class="center"><a href="BuyHistory?buy_id=${bdb.id}" class="btn-floating btn waves-effect waves-light "> <i class="material-icons">details</i></a></td>
										<td class="center">${bdb.buyDate}</td>
										<td class="center">${bdb.totalPrice}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>