<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>選手詳細</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<jsp:include page="/baselayout/head.jsp" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<div class="row col-2 mx-auto">
			<img src="img/${player.fileName}">
			<h5 class=" col s12 light">${player.name}</h5>
		</div>
		<div class="section">
			<!--   商品情報   -->
			<div class="row">
					<div class="col s12 m3">
						<div class="card">
							<div class="card-image">
								<a href="ItemDetail?item_id=${item.id}&page_num=${pageNum}"><img
									src="img/${item.fileName}"></a>
							</div>
							<div class="card-content">
								<span class="card-title">${item.name}</span>
								<p>${item.price}円</p>
							</div>
						</div>
					</div>
			</div>
		</div>
		<a href="PlayerList">戻る</a>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>