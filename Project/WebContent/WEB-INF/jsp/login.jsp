<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	 crossorigin="anonymous">
<jsp:include page="/baselayout/head.jsp" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<c:if test="${errMsg != null}" >
	   		<div class="alert alert-danger" role="alert">
		 	 ${errMsg}
			</div>
		</c:if>
	<div class="container">
		<div class="row">
			<div class="section"></div>
			<div class="col s6 offset-s3">
				<div class="card grey lighten-5">
					<div class="card-content">
						<form action="LoginServlet" method="POST">
							<div class="row col-4 mx-auto">
								<div class="input-field col s8 offset-s2">
									<input type="text" name="loginId" value="${inputLoginId}" required> <label>ログインID</label>
								</div>
							</div>
							<div class="row col-4 mx-auto">
								<div class="input-field col s8 offset-s2">
									<input type="password" name="password" required> <label>パスワード</label>
								</div>
							</div>
							<div class="row col-3 mx-auto">
								<div class="col s12">
									<p class="center-align">
										<button type="submit" class="btn btn-info">ログイン</button>
									</p>
								</div>
							</div>
							<div class="row col-md-6 offset-md-6">
								<div class="col s8 offset-s2">
									<p class="right-align">
										<a href="RegistServlet">新規登録</a>
									</p>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>