<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>検索結果</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<jsp:include page="/baselayout/head.jsp" />
</head>

<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<div class="row col-4 mx-auto">
			<div class="input-field col s8 offset-s2 ">
				<form action="PlayerList">
					<input type="text" name="search_word" value="${searchWord}">
					<button class="btn btn-info" type="submit" name="action">検索</button>
				</form>
			</div>
		</div>
		<div class="row col-2 mx-auto">
			<h5 class=" col s12 light">検索結果</h5>
		</div>
		<div class="row col-3 mx-auto">
			<p>検索結果${playerCount}件</p>
		</div>

		<div class="section">
			<!--   商品情報   -->
			<div class="row">
				<table class="table">
					<thead class="thead-dark">
						<tr>
							<th scope="col"></th>
							<th scope="col">選手名</th>
							<th scope="col">所属チーム</th>
							<th scope="col">契約メーカー</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="player" items="${playerList}">
							<tr>
								 <td><a href="PlayerDetail?id=${player.id}"><img src="img/${player.fileName}"></a></td>
								<td><a href="PlayerDetail?id=${player.id}">${player.name}</a></td>
								<td>${player.teamId}</td>
								<td>${player.manufacturerId}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row col-2 mx-auto">
			<ul class="pagination">
					<!-- １ページ戻るボタン  -->
				<c:choose>
					<c:when test="${pageNum == 1}">
						<li class="disabled"><a><i class="material-icons">chevron_left</i></a></li>
					</c:when>
					<c:otherwise>
						<li class="waves-effect"><a href="PlayerList?search_word=${searchWord}&page_num=${pageNum - 1}"><i class="material-icons">chevron_left</i></a></li>
					</c:otherwise>
				</c:choose>

				<!-- ページインデックス -->
				<c:forEach begin="${(pageNum - 5) > 0 ? pageNum - 5 : 1}" end="${(pageNum + 5) > pageMax ? pageMax : pageNum + 5}" step="1" varStatus="status">
					<li <c:if test="${pageNum == status.index }"> class="active" </c:if>><a href="PlayerList?search_word=${searchWord}&page_num=${status.index}">${status.index}</a></li>
				</c:forEach>

				<!-- 1ページ送るボタン -->
				<c:choose>
				<c:when test="${pageNum == pageMax || pageMax == 0}">
					<li class="disabled"><a><i class="material-icons">chevron_right</i></a></li>
				</c:when>
				<c:otherwise>
					<li class="waves-effect"><a href="PlayerList?search_word=${searchWord}&page_num=${pageNum + 1}"><i class="material-icons">chevron_right</i></a></li>
				</c:otherwise>
				</c:choose>
			</ul>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>

</html>