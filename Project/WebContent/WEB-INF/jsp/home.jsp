<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
	<title>TOPページ</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
		integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<jsp:include page="/baselayout/head.jsp" />
</head>

<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<br> <br>
		<div class="row col-7 mx-auto">
			<h3 class="header center red-text">サッカー選手商品サイト</h3>
		</div>
		<div class="row center">
			<h5 class="header col s12 light">選手関連の商品が買えるサイト</h5>
		</div>
		<div class="row col-5 mx-auto">
			<div class="input-field col s8 offset-s2">
				<form action="PlayerList">
					<input type="text" name="search_word">
					<button class="btn btn-info" type="submit" name="action">検索</button>
				</form>
			</div>
		</div>
		<br> <br>
		<div class="row center">
			<h5 class=" col s12 light">人気選手</h5>
		</div>
		<div class="section">
			<!--   人気選手   -->
			<div class="row col-11 mx-auto">
				<c:forEach var="player" items="${playerList}">
					<div class="col s12 m3">
						<div class="card">
							<div class="card-image">
								<a href="PlayerDetail?id=${player.id}"><img src="img/${player.fileName}"></a>
							</div>
							<div class="card-content">
								<span class="card-title">${player.name}</span>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>

</html>