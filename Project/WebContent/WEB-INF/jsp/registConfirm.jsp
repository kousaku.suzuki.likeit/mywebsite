<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー登録/入力内容確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	 crossorigin="anonymous">
<jsp:include page="/baselayout/head.jsp" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<br>
	<div class="container">
		<div class="row col-4 mx-auto">
			<h5 class=" col s12 light">入力内容確認</h5>
		</div>
		<div class="row">
			<div class="section"></div>
			<div class="col s6 offset-s3">
				<div class="card grey lighten-5">
					<div class="card-content">
						<form action="RegistResultServlet" method="POST">
							<div class="row col-5 mx-auto">
								<div class="input-field col s10 offset-s1">
									<label>名前　　　</label><input type="text" name="name" value="${udb.name}" readonly>
								</div>
							</div>
							<div class="row col-5 mx-auto">
								<div class="input-field col s10 offset-s1">
									<label>住所　　　</label><input type="text" name="address" value="${udb.address}" readonly>
								</div>
							</div>
							<div class="row col-5 mx-auto">
								<div class="input-field col s10 offset-s1">
									<label>ログインID</label><input type="text" name="login_id" value="${udb.loginId}" readonly>
								</div>
							</div>
							<div class="row col-5 mx-auto">
								<div class="input-field col s10 offset-s1">
									<label>パスワード</label><input type="password" name="password" value="${udb.password}" readonly>
								</div>
							</div>
							<div class="row col-5 mx-auto">
								<div class="col s12">
									<p class="center-align">上記内容で登録してよろしいでしょうか?</p>
								</div>
							</div>
							<div class="row col-5 mx-auto">
								<div class="col s6 center-align">
                                    <button class="btn btn-info" type="submit" name="confirm_button" value="cancel">修正</button>
								</div>
								<div class="col s6 center-align">
									<button class="btn btn-info" type="submit" name="confirm_button" value="regist">登録</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>