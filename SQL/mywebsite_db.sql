SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `mywebsite` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `mywebsite`;

CREATE TABLE `account` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login_id` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `buy` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `buy_detail` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `buy_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `item` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `price` int(11) DEFAULT NULL,
  `file_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `item` (`id`, `name`, `manufacturer_id`, `price`, `file_name`) VALUES
(1, 'Rp20.1 FG', 1, 13860, 'Rp20.1.jpg'),
(2, 'GbNXS[Xg.1', 1, 13860, 'GbNXS[Xg.jpg'),
(3, 'vf^[ ~[e[^[ 20.1', 1, 13860, 'FW9182.jpg'),
(4, 'vf^[ 20.1', 1, 13860, 'FW9182.jpg'),
(5, 'eBG|WFh8', 2, 25300, 'AT5293-030.jpg'),
(6, 'eBG|WFh8 G[g', 2, 25300, 'AT5293-303.jpg'),
(7, '}[LA FCp[14', 2, 20790, 'CZ8717-600.jpg'),
(8, 'Eg1.2', 3, 15400, '106340-02.jpg'),
(9, 'Eg1.2 FG/AG', 3, 9240, '106080-03.jpg'),
(10, 'LOv`iFG/AG', 3, 24200, '105606-06.jpg'),
(11, 'DSCgX-FLY4', 4, 19800, '1101A006-119.jpg'),
(12, 'DSCgX-FLY PRO', 4, 22000, '1101A025-100.jpg'),
(13, 'A2JAPAN', 5, 20900, 'P1GA200209.jpg'),
(14, 'A lI2', 5, 20790, 'new_products_neo2_INAZUMA.jpg'),
(15, 'A lI3', 5, 23100, 'P1GA208060.jpg'),
(16, '442 PRO HG', 6, 17380, 'MSCKHWC1-D.jpg'),
(17, 'DSCg ANX', 4, 16500, '1101A017-003.jpg');

CREATE TABLE `player` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `team_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `player` (`id`, `name`, `file_name`, `team_id`, `manufacturer_id`, `item_id`) VALUES
(1, 'Rª', 'yamane_miki.jpg', 1, 4, 11),
(2, 'cÉ', 'TANAKA_Ao.jpg', 1, 5, 13),
(3, 'OâO', 'MITOMA_Kaoru.jpg', 1, 3, 8),
(4, 'eâ×l', 'wakizaka_yasuto.jpg', 1, 3, 10),
(5, 'å»¾', 'member_25147_1.jpg', 1, 5, 13),
(6, 'øèå', 'HATATE_Reo.jpg', 1, 5, 15),
(7, 'Æ·º', 'member_33807_1.jpg', 1, 5, 13),
(8, 'î_Ë', 'inagaki_sho.jpg', 2, 6, 16),
(9, 'nEI', 'SOMA_Yuki.jpg', 2, 5, 14),
(10, '`JjêN', 'member_33975_1.jpg', 2, 2, 6),
(11, 'nÓá©¾', 'WATANABE_Kouta.jpg', 3, 2, 7),
(12, '£Ãà²', 'SEKO_Ayumu.jpg', 4, 2, 5),
(13, 'Ã´æ', 'furuhashi_kyogo.jpg', 5, 4, 12),
(14, 'ìÓx', 'kawabe_hayao.jpg', 6, 1, 1),
(15, '¬ìÈç', 'ogawa_ryoya.jpg', 7, 1, 2),
(16, 'nÓ', 'WATANABE_Tsuyoshi.jpg', 7, 3, 8),
(17, 'cìî', 'TAGAWA_Kyosuke.jpg', 7, 3, 8),
(18, 'ìqÍ', 'member_33989_1.jpg', 8, 1, 3),
(19, ']âC', 'esaka_ataru.jpg', 9, 3, 9),
(20, 'Ãê¾z', 'KOGA_Taiyo.jpg', 9, 5, 15),
(21, '¬c_÷', 'MACHIDA_Koki.jpg', 10, 2, 5),
(22, 'cx¾', 'TANAKA_Shunta.jpg', 11, 5, 15),
(23, '´PãY', 'HARA_Teruki.jpg', 12, 4, 17),
(24, 'F²üMj', 'member_34059_1.jpg', 13, 4, 11);

CREATE TABLE `manufacturer` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `manufacturer` (`id`, `name`) VALUES
(1, 'AfB_X'),
(2, 'iCL'),
(3, 'v[}'),
(4, 'AVbNX'),
(5, '~Ym'),
(6, 'j[oX');

CREATE TABLE `team` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `team` (`id`, `name`) VALUES
(1, 'ìèt^['),
(2, '¼Ã®OpX'),
(3, '¡lFE}mX'),
(4, 'Zb\åã'),
(5, 'BbZ_Ë'),
(6, 'Ttb`FL'),
(7, 'FC'),
(8, 'YabY'),
(9, 'C\'),
(10, '­Ag[Y'),
(11, 'kC¹RTh[Dy'),
(12, '´GXpX'),
(13, 'Koåã');
